package br.com.improving.carrinho;

import java.util.Objects;

/**
 * Classe que representa um produto que pode ser adicionado
 * como item ao carrinho de compras.
 *
 * Importante: Dois produtos são considerados iguais quando ambos possuem o
 * mesmo código.
 */
public class Produto {

    private Long codigo;
	
    private String descricao;

    /**
     * Construtor da classe Produto.
     *
     * @param codigo
     * @param descricao
     */
    public Produto(Long codigo, String descricao) {
    	this.codigo = codigo;
    	this.descricao = descricao;
    }

    /**
     * Retorna o código da produto.
     *
     * @return Long
     */
    public Long getCodigo() {
    	return this.codigo;
    }

    /**
     * Retorna a descrição do produto.
     *
     * @return String
     */
    public String getDescricao() {
    	return this.descricao;
    }
	@Override
	public boolean equals(Object object) {
		if (this == object) {
			return true;
		}
		if (!(object instanceof Produto)) {
			return false;
		}
		if (!super.equals(object)) {
			return false;
		}
		final Produto produto = (Produto) object;
		return java.util.Objects.equals(getCodigo(), produto.getCodigo()) && java.util.Objects.equals(getDescricao(), produto.getDescricao());
	}
	@Override
	public int hashCode() {
		return Objects.hash(super.hashCode(), getCodigo(), getDescricao());
	}

	@Override
	public java.lang.String toString() {
		return "Produto{" +
				"codigo=" + codigo +
				", descricao='" + descricao + '\'' +
				'}';
	}
}