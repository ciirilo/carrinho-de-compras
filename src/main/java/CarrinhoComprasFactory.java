package br.com.improving.carrinho;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Classe responsável pela criação e recuperação dos carrinhos de compras.
 */
public class CarrinhoComprasFactory {

	Map<String, CarrinhoCompras> carrinhosCompras = new HashMap<String, CarrinhoCompras>();

	/**
	 * Cria um carrinho de compras para o cliente passado como parÃ¢metro.
	 *
	 * Caso jÃ¡ existe um carrinho de compras para o cliente passado como parÃ¢metro, esse carrinho deverÃ¡ ser retornado.
	 *
	 * @param identificacaoCliente
	 * @return CarrinhoCompras
	 */
	public CarrinhoCompras criar(String identificacaoCliente) {
		CarrinhoCompras carrinhoCompras = new CarrinhoCompras();

		if(carrinhosCompras.containsKey(identificacaoCliente)) {
			carrinhoCompras = null;
		} else {
			carrinhosCompras.put(identificacaoCliente, carrinhoCompras);
		}

		return carrinhoCompras;
	}

	/**
	 * Retorna o valor do ticket mÃ©dio no momento da chamada ao mÃ©todo.
	 * O valor do ticket mÃ©dio Ã© a soma do valor total de todos os carrinhos de compra dividido
	 * pela quantidade de carrinhos de compra.
	 * O valor retornado deverÃ¡ ser arredondado com duas casas decimais, seguindo a regra:
	 * 0-4 deve ser arredondado para baixo e 5-9 deve ser arredondado para cima.
	 *
	 * @return BigDecimal
	 */
	public BigDecimal getValorTicketMedio() {
		List<CarrinhoCompras> carrinhoComprasL = new ArrayList<>(carrinhosCompras.values());

		carrinhoComprasL.stream()
				.forEach(s -> s.getValorTotal().plus().divide(new BigDecimal(carrinhoComprasL.size())));
		BigDecimal valorTicketMedio = (BigDecimal) carrinhoComprasL;
		return valorTicketMedio.setScale(2, RoundingMode.HALF_EVEN);
	}

	/**
	 * Invalida um carrinho de compras quando o cliente faz um checkout ou sua sessÃ£o expirar.
	 * Deve ser efetuada a remoÃ§Ã£o do carrinho do cliente passado como parÃ¢metro da listagem de carrinhos de compras.
	 *
	 * @param identificacaoCliente
	 * @return Retorna um boolean, tendo o valor true caso o cliente passado como parÃ¤metro tenha um carrinho de compras e
	 * e false caso o cliente nÃ£o possua um carrinho.
	 */
	public boolean invalidar(String identificacaoCliente) {
		try {
			carrinhosCompras.remove(identificacaoCliente);
			return true;
		}
		catch(RuntimeException e) {
			return false;
		}
	}
}
